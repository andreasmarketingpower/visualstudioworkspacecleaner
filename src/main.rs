use walkdir::{DirEntry, WalkDir};

gflags::define! {
    /// Specify the root_dir to the Visual Studio projects
    -r, --root_dir = ""
}

gflags::define! {
    /// Simulate only prints the folders to be deleted but does not do anything
    -s, --simulate = false
}

gflags::define! {
    /// Shows this screen
    -h, --help = false
}

fn should_display(e: &DirEntry) -> bool {
    let metadata = e.metadata();
    if metadata.is_err() {
        return false;
    }

    if !metadata.unwrap().is_dir() {
        return false;
    }

    let filename = e.file_name().to_str().unwrap_or("");
    if filename == "packages" || filename == "node_modules" {
        return false;
    }

    if filename.starts_with('.') {
        return false;
    }

    true
}

fn main() {
    gflags::parse();

    if HELP.flag || !ROOT_DIR.is_present() {
        gflags::print_help_and_exit(1);
    }

    let walker = WalkDir::new(ROOT_DIR.flag);
    for entry in walker.into_iter().filter_entry(|e| should_display(e)) {
        let entry = entry.unwrap();

        let sub_walker = WalkDir::new(entry.path()).max_depth(1);
        let dirs: Vec<DirEntry> = sub_walker
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.metadata().unwrap().is_dir())
            .filter(|e| {
                let filename = e.path().file_name().and_then(|e| e.to_str()).unwrap_or("");

                filename == "bin" || filename == "obj"
            })
            .collect();

        if dirs.len() != 2 {
            continue;
        }

        for dir in dirs {
            print!("{} ... ", &dir.path().display());

            if !SIMULATE.flag
                && !dir
                    .path()
                    .file_name()
                    .unwrap_or_default()
                    .to_str()
                    .unwrap_or_default()
                    .is_empty()
            {
                if std::fs::remove_dir_all(&dir.path()).is_ok() {
                    print!("deleted");
                } else {
                    print!("failed to remove");
                }
            } else {
                print!("found");
            }

            println!();
        }
    }
}
