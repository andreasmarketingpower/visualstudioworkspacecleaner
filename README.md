# Visual Studio Workspace Cleaner

Sometimes when switching between framework versions then Visual Studio 2017/2019 fails to remove the folders bin\ and also obj\. This rust cli tool will look for these folders and remove them if both exist.

**Use it at your own Risk. It might delete your entire hard drive if not used properly**
